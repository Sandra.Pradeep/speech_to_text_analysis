ROS SPEECH TO SENTIMENTAL USING PUBLISHER AND SUBSCRIBER(PYTHON)

Project description:
             Its a ROS publisher and subscriber program for converting speech to text and to perform sentimental analysis on the text, to check whether the code belongs to positive, negative or neutral word. Here i used a recorded audio file as source. You can also use microphone as your input source. 

You need to  install ros in your Linux os ( i’m using ubuntu 16.04) by using this link: http://wiki.ros.org/kinetic/Installation/Ubuntu

Requirements:

1.Pip

Run the following command to update the package list and upgrade all of your system software to the latest version available

sudo apt-get update && sudo apt-get -y upgrade

Then install pip 

sudo apt-get install python-pip

2.PyAudio :

sudo apt-get install portaudio19-dev python-all-dev python3-all-dev && sudo pip install pyaudio

3.SpeechRecognition 3.8.1:

pip install SpeechRecognition

Follow this link for more deatils:https://pypi.org/project/SpeechRecognition/

After installing all the above things
follow these steps:

1.Create a catkin workspace
 Follow the link: http://wiki.ros.org/catkin/Tutorials/create_a_workspace

2.Create a catkin Package
 Follow the link: http://wiki.ros.org/ROS/Tutorials/CreatingPackage

3.Writing a  Publisher and Subscriber
 Follow the link :http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28python%29

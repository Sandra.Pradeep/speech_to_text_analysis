#!/usr/bin/env python
# license removed for brevity

import rospy 
from std_msgs.msg import String
import speech_recognition as sr

def speech2txt():
#Creating publisher node
    pub = rospy.Publisher('word', String, queue_size=10)
#Create node with name speechnode. 
# In ROS, nodes are uniquely named. If two nodes with the same
# name are launched, the previous one is kicked off. The
# anonymous=True flag means that rospy will choose a unique
# name for our 'text2sentimental' node so that multiple listeners can
# run simultaneously.
    rospy.init_node('speechnode', anonymous=True) 
    rate = rospy.Rate(10) # 10hz
##Speech recognition function
    r = sr.Recognizer()

##For Using microphone as the source of voice, uncomment these lines below
   #with sr.Microphone() as source:
        #Reads the audio file
        #print("say something")
        #audio = r.listen(source) 

#when microphone does not work we can run the program 
#by using a previously recorded audio file
    with sr.WavFile("Recording_3.wav") as source:
         audio=r.record(source)

#Execute program until there is an interrupt ctrl-c   

    while not rospy.is_shutdown():
#Audio is converted into text
        y=r.recognize_google(audio)
#Messages get printed to screen.
        rospy.loginfo(y)
#Publishes a string to our word topic.
        pub.publish(y)
        rate.sleep()

# Start main function 
if __name__ == '__main__':
# calling speech2txt function
       try:
          speech2txt()
       except rospy.ROSInterruptException:
               pass

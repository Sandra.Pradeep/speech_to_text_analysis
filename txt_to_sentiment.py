#!/user/bin/env python
# license removed for brevity

import rospy
from std_msgs.msg import String
#Defining list  for sentimental analysis of converted text
#Listing some postive words
positive=['happy','smile','beautiful','wonderful']
#Listing some negative words
negative=['sad','cry','sorrow','criminal']
#Listing some nuetral words
neutral=['hello','hai','place','day']

def callback(data):
    rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)
    y=data.data
#creating a publisher node
    pub = rospy.Publisher('word', String, queue_size=10)
    rospy.init_node('text2sentiment', anonymous=True) 
#Check for the presence of published data in positive list
    for i in positive:
      if i==y:
        #rospy.loginfo('The word is positive')
        pub.publish('The word is positive')
#Check for the presence of published data in negative list
    for i in negative:
      if i==y:
        #rospy.loginfo('The word is negative')
        pub.publish('The word is negative')
#Check for the presence of published data in neutral list    
    for i in neutral:
      if i==y:
        #rospy.loginfo('The word is neutral')
        pub.publish('The word is nuetral')
 
def listener():

# In ROS, nodes are uniquely named. If two nodes with the same
# name are launched, the previous one is kicked off. The
# anonymous=True flag means that rospy will choose a unique
# name for our 'text2sentimental' node so that multiple listeners can
# run simultaneously.

    rospy.init_node('text2sentiment', anonymous=True)
    rospy.Subscriber('word', String,callback)
# spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
    

#Start of main function
if __name__ == '__main__':
    y=listener()

